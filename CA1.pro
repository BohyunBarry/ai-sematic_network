%Frame mammal: base class for the animals in our game

%Frame cat: base class for cats in the game
cat(subclass, mammal).
cat(chases, mouse).
cat(sleeps, cat_bed).
cat(has_part, [fur, tail, whiskers]).
cat(eats, cat_food).

%Frame mouse: base class for mice in the game
mouse(subclass, mammal).
mouse(eats, cheese).
mouse(has_part, [feet, fur, whiskers]).
mouse(sleeps, mouse_hole).
mouse(colour, brown).

%Frame mtspet: subclass that defines what a pet of Momma Two Shoes is e.g. Tom
mtspet(eats, cat_food).
mtspet(sleeps, stairs).

%Frame tom: instance of Tom the cat
tom(instance_of, cat).
tom(subclass, mtspet).
tom(colour, blue).

%Frame jerry: instance of Jerry the mouse
jerry(instance_of, mouse).
jerry(sleeps*, sofa).

predecessor(x,y) :- is_a(x,y).
predecessor(x,y) :- is_a(x,p),
        is_a(p,y).

instance_of(obj,class):- is_a(obj,class).
instance_of(obj,class):- is_a(obj,class_1), subclass(class_1,class).
subclass(class_1,class_2):- ako(class_1,class_2).
subclass(clas_1,class_2):- ako(class_1,class_3), subclass(class_3,class_2).

value(Frame, Slot, Value) :- 
    Query =.. [Frame, Slot, Value],
    call(Query),!.
    
value(Frame, Slot, Value) :-
    Slot == sleeps
    ->
    parent(Frame, ParentFrame),
    value(ParentFrame, Slot, Value),!
    ;
    parent(Frame, ParentFrame),
    value(ParentFrame, Slot, Value).

parent(Frame, ParentFrame) :-
    (Query =.. [Frame, subclass, ParentFrame]
    ;
    Query =.. [Frame, instance_of, ParentFrame]),
    call(Query).
